package org.needcoke.coke.web.exception;

public class ExceptionAdviceError extends RuntimeException{

    public ExceptionAdviceError(String message) {
        super(message);
    }
}
