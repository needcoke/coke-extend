package org.needcoke.coke.kafka.exc;

public class TopicException extends RuntimeException{

    public TopicException(String message) {
        super(message);
    }
}
