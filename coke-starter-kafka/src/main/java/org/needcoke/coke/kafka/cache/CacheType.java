package org.needcoke.coke.kafka.cache;

public enum CacheType {

    PRODUCER,  // 生产者
    CONSUMER,  // 消费者
}
