package org.needcoke.coke.aop.proxy;

public enum AdviceType {

    BEFORE, AFTER, AFTER_EXCEPTION, AFTER_RETURNING
}
