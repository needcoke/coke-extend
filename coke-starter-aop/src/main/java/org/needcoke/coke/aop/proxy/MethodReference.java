package org.needcoke.coke.aop.proxy;

import java.io.Serializable;
import java.util.function.Function;

public interface MethodReference extends Function, Serializable {
}
